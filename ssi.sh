#!/usr/bin/env bash
# PATH=/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin:~/bin
# export PATH

ssr_file="/usr/local/shadowsocksr"

cd `dirname $0`
file_path=`pwd`
echo file_path

check_sys(){
	if [[ -f /etc/redhat-release ]]; then
		release="centos"
	elif cat /etc/issue | grep -q -E -i "debian"; then
		release="debian"
	elif cat /etc/issue | grep -q -E -i "ubuntu"; then
		release="ubuntu"
	elif cat /etc/issue | grep -q -E -i "centos|red hat|redhat"; then
		release="centos"
	elif cat /proc/version | grep -q -E -i "debian"; then
		release="debian"
	elif cat /proc/version | grep -q -E -i "ubuntu"; then
		release="ubuntu"
	elif cat /proc/version | grep -q -E -i "centos|red hat|redhat"; then
		release="centos"
    fi
	#bit=`uname -m`
}

debian_apt(){
	apt-get update
	apt-get install -y python-pip python-m2crypto curl unzip vim git gcc build-essential make
	apt-get install build-essential
	cd ${file_path}
	wget https://github.com/jedisct1/libsodium/releases/download/1.0.10/libsodium-1.0.10.tar.gz
	tar xf libsodium-1.0.10.tar.gz && cd libsodium-1.0.10
	./configure && make -j2 && make install
	ldconfig
}
centos_yum(){
	yum install epel-release
	yum update
	yum install -y python-pip python-m2crypto curl unzip vim git gcc make
	yum install libsodium
}


createInitFile(){
	cp -f ${file_path}/shadowsocks /etc/init.d/shadowsocks
	# chmod a+x /etc/init.d/shadowsocks
	if [[ ${release} = "debian" ]]; then
		chmod 755 /etc/init.d/shadowsocks ; update-rc.d shadowsocks defaults ; service shadowsocks start
	elif [[ ${release} = "ubuntu" ]]; then
		chmod 755 /etc/init.d/shadowsocks ; update-rc.d shadowsocks defaults ; service shadowsocks start
	elif [[ ${release} = "centos" ]]; then
		chmod 755 /etc/init.d/shadowsocks && chkconfig --add shadowsocks && service shadowsocks start
	else
		echo -e "\033[41;37m [错误] \033[0m 本脚本不支持当前系统 !" && exit 1
	fi
}

installSS(){
	check_sys
	# 系统判断
	if [[ ${release} = "debian" ]]; then
		debian_apt
	elif [[ ${release} = "ubuntu" ]]; then
		debian_apt
	elif [[ ${release} = "centos" ]]; then
		centos_yum
	else
		echo -e "\033[41;37m [错误] \033[0m 本脚本不支持当前系统 !" && exit 1
	fi
	#修改DNS为8.8.8.8
	echo "nameserver 8.8.8.8" > /etc/resolv.conf
	echo "nameserver 8.8.4.4" >> /etc/resolv.conf
	cp -f /usr/share/zoneinfo/Asia/Shanghai /etc/localtime
	
	# cd /usr/local/
	apt-get install python-pip
	pip install git+https://github.com/shadowsocks/shadowsocks.git@master
	# cd ./shadowsocksr
	cp -f ${file_path}/shadowsocks.json /usr/local/shadowsocksr/shadowsocks.json
	createInitFile
}

installSS
echo "SS install done."